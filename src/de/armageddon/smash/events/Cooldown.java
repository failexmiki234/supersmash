package de.armageddon.smash.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.scheduler.BukkitTask;

import de.armageddon.smash.SuperSmash;

public class Cooldown extends PlayerEvent {

	private Player player;
	private int seconds;
	BukkitTask coold;
	SuperSmash su = new SuperSmash();
	private static final HandlerList handlers = new HandlerList();

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public Cooldown(Player player, int seconds) {
		super(player);
		this.player = player;
		this.seconds = seconds;
	}

	public void cooldown() {
		coold = Bukkit.getScheduler().runTaskTimerAsynchronously(su, new Runnable() {

			@Override
			public void run() {
				player.setLevel(su.prozent);
				su.prozent += 1;
				if (su.prozent == 100) {
				}
			}
			
			
		}, 1, 1);
	}


}
