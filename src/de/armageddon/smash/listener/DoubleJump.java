package de.armageddon.smash.listener;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.craftbukkit.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.util.Vector;

import de.armageddon.smash.SuperSmash;

public class DoubleJump implements Listener {

	SuperSmash su = new SuperSmash();

	@EventHandler
	public void onPlayerToggleFly(PlayerToggleFlightEvent e) {
		Player p = e.getPlayer();
		if ((p.getGameMode() == GameMode.CREATIVE) || (p.getGameMode() == GameMode.SPECTATOR)) {
			return;
		}
		e.setCancelled(true);
		p.setFlying(false);
		p.setAllowFlight(false);
		p.setFallDistance(0.0F);
		Location loc = p.getLocation();
		Vector v = loc.getDirection().multiply(1.2F).setY(1.2D);
		p.setVelocity(v);
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		if ((p.getGameMode() == GameMode.CREATIVE) || (p.getGameMode() == GameMode.SPECTATOR)) {
			return;
		}
		if ((p.isOnGround()) && (!p.getAllowFlight()) || (!p.getAllowFlight())) {
			if (su.cooldown.contains(p)) {
				p.sendMessage(su.pr + "Du musst warten, bis dein Double-Jump aufgeladen ist!");
			} else {
				p.setAllowFlight(true);
			}
		}
	}
}
