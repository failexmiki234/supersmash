package de.armageddon.smash;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import de.armageddon.smash.listener.DoubleJump;
import de.armageddon.smash.listener.StuffListener;

public class SuperSmash extends JavaPlugin {

	public Gamestate state;
	
	public ArrayList<Player> cooldown;
	public String pr;
	public String noperm;
	public String gamestart;
	public String death;
	public String powerup;
	public SuperSmash main;
	public int prozent;
	
	@Override
	public void onEnable() {
		cooldown = new ArrayList<Player>();
		Bukkit.getServer().getPluginManager().registerEvents(new DoubleJump(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new StuffListener(), this);
	}
}
